<?php
 
	$this->load_template('header.php');
	
?>
<link href="//font.cgi-bin.ir/type_css/r_b_yekan" rel="stylesheet" type="text/css">
<style>
#number {
	width:250px;
	padding:3px;
	text-align:center;
	border:1px #ccc solid;
	font:12pt b_yekan;
}
</style>
					<div id="settings">
						<div id="settings_left">
							<?php $this->load_template('settings_leftmenu.php') ?>
						</div>
						<div id="settings_right">
							<div class="ttl"><div class="ttl2">
								<h3>بخش ثبت و مدیریت شماره موبایل</h3>
							</div></div>
                            <? if(!$D->SMS == '1') { ?>
                            <div style="padding:10px;font:10pt 'b_yekan'; color:#666;">
							این بخش توسط تیم مدیریت سایت غیر فعال شده است . <br /> برای اطلاعات بیشتر با مدیریت در تماس باشید.
                            </div>
							<? } else { ?>
							<div style="padding:10px;font:10pt 'b_yekan'; color:#666;">
                            لیست امکانات سرویس :
                            <li>ارسال پست و نظر با استفاده از سرویس پیامک سایت</li>
                            <li>تغییر اطلاعات کاربری با استفاده از سرویس پیامک سایت</li>
                            <li>ارسال پست و نظر به گروه ها با استفاده از سرویس پیامک سایت</li>
                            <li>ارسال پیامک به شما از سوی کاربران شبکه با توجه به موجودی حساب کاربری</li>
                            <li>ارسال پیامک به شما از طریق گروه پشتیبانی سایت برای معرفی امکانات و اطلاع رسان ها</li>
                            <span style="color:#933;">توجه داشته باشید پیامک های ارسالی شما توسط تیم مدیریت سایت بررسی شده و اگر خلاف قوانین باشد پیگرد قانونی خواهد داشت.<br />سرویس پیامک این سایت توسط سید امیر حسین طاووسی طراحی و برنامه نویسی شده است.</span>
                            <hr style="border:1px #666 solid;" />
                            <div id="alertt">
                            <div id="details"></div>
                            </div>
                            <div id="mobile_active">
                            <? if(empty($this->user->info->mobile)) { ?>
                            <table>
                             <tr>
                              <td id="mobile_title" style="font:10pt 'b_yekan';">شماره موبایل شما : </td>
                              <td id="numb"><input type="text" id="number" placeholder="0915" value="" /></td>
                             </tr>
                             <tr>
                              <td></td>
                              <td>
                              <button onclick="mobile_submit()" id="sub" style="font:10pt 'b_yekan';">ثبت و ارسال پیامک فعال سازی</button>
                              <img style="display:none;" id="ecld" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/fbloader.gif" />
                              </td>
                             </tr>
                            </table>   
                            <? } else if(empty($this->user->info->mobile_active)) { ?>
                            <span style="font:10pt 'b_yekan';">شماره موبایل ثبت شده : <?= $this->user->info->mobile ?></span>
                            <table>
                             <tr>
                              <td id="mobile_title" style="font:10pt 'b_yekan';">کد فعال سازی شما : </td>
                              <td><input type="text" id="number" 
                              name="mobile_number" /></td>
                             </tr>
                             <tr>
                              <td></td>
                              <td>
                              <button onclick="mobile_submit()" style="font:10pt 'b_yekan';">ثبت کد فعال سازی</button>
                              <img style="display:none;" id="ecld" src="<?= $C->SITE_URL.'themes/'.$C->THEME ?>/imgs/fbloader.gif" />
                              </td>
                             </tr>
                            </table>   
                            <? } else { ?>
                            <div style="padding:10px;font:10pt b_yekan; color:#666;">
                             سرویس پیامک اکانت شما تایید شده است . هم اکنون می توانید از این سرویس استفاده کنید.<br />
                             شماره موبایل ثبت شده : <?= $this->user->info->mobile ?>
                            </div>
                            <? } ?>
                            </div>
                            </div>
                            <? } ?>
						</div>
					</div>
<?php
	
	$this->load_template('footer.php');
	
?>