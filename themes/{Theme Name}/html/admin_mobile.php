<?php
	
	$this->load_template('header.php');
	
?>
    <script>
      function countChar(val) {
        var len = val.value.length;
        if (len >= 160) {
          val.value = val.value.substring(0, 160);
        } else {
          $('#charNum').text(160 - len);
        }
      };
    </script>
					<div id="settings">
						<div id="settings_left">
							<?php $this->load_template('admin_leftmenu.php') ?>
						</div>
						<div id="settings_right">
							<div class="ttl">
								<div class="ttl2">
									<h3>تنظیمات سرویس پیامک سایت</h3>
								</div>
							</div>
							<?php if($D->error) { ?>
							<?= errorbox($this->lang('admgnrl_error'), $this->lang($D->errmsg,array('#SITE_TITLE#'=>$C->OUTSIDE_SITE_TITLE)), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
							<?php } elseif($D->submit) { ?>
							<?= okbox($this->lang('admgnrl_okay'), $this->lang('admgnrl_okay_txt'), TRUE, 'margin-top:5px;margin-bottom:5px;') ?>
							<?php } ?>
							<div class="greygrad" style="margin-top:5px;">
								<div class="greygrad2">
									<div class="greygrad3" style="padding-top:10px; line-height:22px;">
  به دلیل اینکه این صفحه را فقط مدیران می توانند مشاهده کنند محدودیتی در وارد کردن اطلاعات در فیلدها گذاشته نشده است .
  <br /> راهنما : این سیستم کمک می کند تا متن پیامک فعال سازی بیش از 160 کارکتر یا یک پیامک نشود در صورتی که میخواهید متن شما بیش از یک پیامک شود  فایل را باز کنید و اعداد 160 که در جاوا اسکریپت اول فایل آمده است را ویرایش کنید .
  <br /> نکته دیگر ان است که توجه داشته باشید 3 متغیر اصلی در متن پیامک وجود دارد ممکن است در بعضی مواقع متغیرها طولانی شوند به همین دلیل توصیه می کنم متغیری که نام فرد را نشان می دهد از داخل متن حذف کنید.
										<form method="post" action="">
											<table id="setform" cellspacing="5">
												<tr>
													<td class="setparam">نام کاربری پنل پیامک</td>
													<td><input type="text" name="IRANSMSPANEL_USERNAME" value="<?= htmlspecialchars($D->IRANSMSPANEL_USERNAME) ?>" class="setinp" dir="ltr" /></td>
												</tr>
												<tr>
													<td class="setparam">رمزعبور پنل پیامک</td>
													<td><input type="text" name="IRANSMSPANEL_PASSWORD" value="<?= htmlspecialchars($D->IRANSMSPANEL_PASSWORD) ?>" class="setinp" dir="ltr" /></td>
												</tr>
												<tr>
													<td class="setparam">شماره اختصاصی شما</td>
													<td><input type="text" name="IRANSMSPANEL_NUMBER" value="<?= htmlspecialchars($D->IRANSMSPANEL_NUMBER) ?>" class="setinp" dir="ltr" /></td>
												</tr>
												<tr>
													<td class="setparam">کلید مخفی پیامک</td>
													<td><input type="text" name="IRANSMSPANEL_ADMIN_KEY" value="<?= htmlspecialchars($D->IRANSMSPANEL_ADMIN_KEY) ?>" dir="ltr" class="setinp" /></td>
												</tr>                                                
                                                <tr>
													<td class="setparam">متن پیامک فعال سازی</td>
													<td><textarea onkeyup="countChar(this)" name="IRANSMSPANEL_TXT" class="setinp"><?= htmlspecialchars($D->IRANSMSPANEL_TXT) ?></textarea>
                                                    <p>هر 160 کارکتر = یک پیامک | شمارنده : <span id="charNum">[برای مشاهده ویرایش کنید]</span></p></td>
												</tr>	
                                                <tr>
													<td class="setparam">وضعیت فلش پیامک</td>
													<td>
                                                    	<select name="IRANSMSPANEL_FLASH" class="setselect">
                                                        <? if($D->IRANSMSPANEL_FLASH == 'true') {
														?>
														<option value="true" selected="selected">فعال</option>
                                                        <option value="false">غیر فعال</option>
                                                        <? }  else  { ?>
                                                        <option value="true">فعال</option>
                                                        <option value="false" selected="selected">غیر فعال</option>
                                                        <? } ?>
														</select>
                                                    </td>
												</tr>												<tr>
													<td class="setparam">وضعیت سرویس</td>
													<td>
                                                    	<select name="SMS" class="setselect">
                                                        <? if($D->SMS == '1') {
														?>
														<option value="1" selected="selected">فعال</option>
                                                        <option value="0">غیر فعال</option>
                                                        <? }  else  { ?>
                                                        <option value="1">فعال</option>
                                                        <option value="0" selected="selected">غیر فعال</option>
                                                        <? } ?>
														</select>
                                                    </td>
												</tr>
												<tr>
													<td></td>
													<td><input type="submit" name="sbm" value="<?= $this->lang('admgnrl_frm_sbm') ?>" style="padding:4px; font-weight:bold;"/></td>
												</tr>
											</table>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
<?php
	
	$this->load_template('footer.php');
	
?>