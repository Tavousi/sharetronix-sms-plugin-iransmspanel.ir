<?php
	// Sharetronix SMS Plugin !
	// By Seyed AmirHosein Tavousi
	// WWW.BLOG.TAVOUSI.NAME
	if( !$this->network->id ) {
		$this->redirect('home');
	}
	if( !$this->user->is_logged ) {
		$this->redirect('signin');
	}
	$db2->query('SELECT 1 FROM users WHERE id="'.$this->user->id.'" AND is_network_admin=1 LIMIT 1');
	if( 0 == $db2->num_rows() ) {
		$this->redirect('dashboard');
	}
	
	$this->load_langfile('inside/global.php');
	$this->load_langfile('inside/admin.php');
		
	$s	= new stdClass;
	$db2->query('SELECT word, value FROM settings');
	while($o = $db2->fetch_object()) {
		$s->{stripslashes($o->word)}	= stripslashes($o->value);
	}
	$D->SMS	                   = $s->SMS;
	$D->IRANSMSPANEL_ADMIN_KEY	= $s->IRANSMSPANEL_ADMIN_KEY;
	$D->IRANSMSPANEL_USERNAME	 = $s->IRANSMSPANEL_USERNAME;
	$D->IRANSMSPANEL_PASSWORD	 = $s->IRANSMSPANEL_PASSWORD;
	$D->IRANSMSPANEL_NUMBER       = $s->IRANSMSPANEL_NUMBER;
	$D->IRANSMSPANEL_FLASH	    = $s->IRANSMSPANEL_FLASH;
	$D->IRANSMSPANEL_TXT	      = $s->IRANSMSPANEL_TXT;
			
	$D->submit	= FALSE;
	$D->error	= FALSE;
	$D->errmsg	= '';
	if( isset($_POST['sbm']) ) {
		$D->submit	= TRUE;
		    $D->SMS	= trim($_POST['SMS']);
			$D->IRANSMSPANEL_ADMIN_KEY	= trim($_POST['IRANSMSPANEL_ADMIN_KEY']);
			$D->IRANSMSPANEL_USERNAME     = trim($_POST['IRANSMSPANEL_USERNAME']);
			$D->IRANSMSPANEL_PASSWORD	 = trim($_POST['IRANSMSPANEL_PASSWORD']);
			$D->IRANSMSPANEL_NUMBER	   = trim($_POST['IRANSMSPANEL_NUMBER']);
			$D->IRANSMSPANEL_FLASH	    = trim($_POST['IRANSMSPANEL_FLASH']);
			$D->IRANSMSPANEL_TXT	      = trim($_POST['IRANSMSPANEL_TXT']);
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_ADMIN_KEY", value="'.$db2->e($D->IRANSMSPANEL_ADMIN_KEY).'" ');			
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_USERNAME", value="'.$db2->e($D->IRANSMSPANEL_USERNAME).'" ');
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_PASSWORD", value="'.$db2->e($D->IRANSMSPANEL_PASSWORD).'" ');
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_NUMBER", value="'.$db2->e($D->IRANSMSPANEL_NUMBER).'" ');
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_FLASH", value="'.$db2->e($D->IRANSMSPANEL_FLASH).'" ');
			$db2->query('REPLACE INTO settings SET word="IRANSMSPANEL_TXT", value="'.$db2->e($D->IRANSMSPANEL_TXT).'" ');
			$db2->query('REPLACE INTO settings SET word="SMS", value="'.$db2->e($D->SMS).'" ');
		    $this->network->load_network_settings();
	}
	$D->page_title = 'مدیریت سامانه پیامک - '.$C->SITE_TITLE;
	$this->load_template('admin_mobile.php');
	
?>